package br.com.edmilsonneto.myfitevolution.activities;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import br.com.edmilsonneto.myfitevolution.R;
import br.com.edmilsonneto.myfitevolution.fragments.ExemploFragment;
import br.com.edmilsonneto.myfitevolution.fragments.MainFragment;
import br.com.edmilsonneto.myfitevolution.utils.Utils;

public class MainActivity extends AppCompatActivity {

    private static FragmentManager fragmentManager;
    private static Class lastFragment;
    private static DrawerLayout drawer;
    private static Class ultimoFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.setDrawerListener(toggle);
        toggle.syncState();

        fragmentManager = getSupportFragmentManager();

        NavigationView nav_view = (NavigationView) findViewById(R.id.nav_view);
        View headerLayout = nav_view.getHeaderView(0);
        generateMenu(headerLayout);

        if (savedInstanceState == null) {
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.add(R.id.container, new MainFragment(), MainFragment.class.getSimpleName());
            ft.addToBackStack(MainFragment.class.getSimpleName());
            ft.commit();
        }
    }

    public static void ChangeFragment(Fragment fragment) {
        FragmentTransaction ft = fragmentManager.beginTransaction();

        drawer.closeDrawer(GravityCompat.START);

        if (lastFragment == null || fragment.getClass() != lastFragment) {
            ft.add(R.id.container, fragment, fragment.getClass().getSimpleName());
            ft.addToBackStack(fragment.getClass().getSimpleName());
            lastFragment = fragment.getClass();
        }

        ft.commit();
    }

    public static void cleanFragmentStack() {
        while (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStackImmediate();
        }
        lastFragment = null;
    }

    @Override
    public void onBackPressed() {
        if(lastFragment.getSimpleName().equals(MainFragment.class.getSimpleName())){
            finish();
        } else {
            if (!Utils.loadAtivo) {
                int i = fragmentManager.getBackStackEntryCount() - 1;

                if (fragmentManager.getBackStackEntryCount() > 1) {
                    fragmentManager.popBackStack();

                    Fragment fragment = fragmentManager.findFragmentByTag(fragmentManager.getBackStackEntryAt(i - 1).getName());

                    while (fragment.getClass() == ultimoFragment) {
                        fragmentManager.popBackStack();
                        i -= 1;
                        fragment = fragmentManager.findFragmentByTag(fragmentManager.getBackStackEntryAt(i - 1).getName());
                    }

                    ultimoFragment = fragment.getClass();
                    ChangeFragment(fragment);
                } else if (fragmentManager.getBackStackEntryCount() == 1) {
                    fragmentManager.popBackStack();
                    ChangeFragment(new MainFragment());
                } else {
                    finish();
                }
            }
        }
    }


    private void generateMenu(View view){
        View btnInicio = view.findViewById(R.id.btnInicio);
        btnInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cleanFragmentStack();
                ChangeFragment(new ExemploFragment());
            }
        });
    }
}
