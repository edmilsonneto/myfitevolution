package br.com.edmilsonneto.myfitevolution.fragments;

import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import br.com.edmilsonneto.myfitevolution.R;
import br.com.edmilsonneto.myfitevolution.utils.Utils;

public class BaseFragment extends Fragment {

    RelativeLayout relativeLayout;

    Resources resources;

    @SuppressWarnings("deprecation")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        resources = getActivity().getResources();

        relativeLayout = new RelativeLayout(getActivity());
        relativeLayout.setVisibility(View.GONE);
        ProgressBar progressBar = new ProgressBar(getActivity());
        relativeLayout.setBackgroundColor(resources.getColor(R.color.loading_bg2));
        relativeLayout.setLayoutParams(new WindowManager.LayoutParams(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN));
        relativeLayout.setClickable(true);
        relativeLayout.setGravity(Gravity.CENTER);
        progressBar.getIndeterminateDrawable().setColorFilter(Color.DKGRAY, android.graphics.PorterDuff.Mode.MULTIPLY);
        relativeLayout.addView(progressBar);
        getActivity().addContentView(relativeLayout, new WindowManager.LayoutParams(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN));
    }

    void ShowLoading() {
        if(relativeLayout != null){
            relativeLayout.setVisibility(View.VISIBLE);
            Utils.loadAtivo = true;
        }

    }

    void HideLoading() {
        if(relativeLayout != null){
            relativeLayout.setVisibility(View.GONE);
            Utils.loadAtivo = false;
        }
    }

}
