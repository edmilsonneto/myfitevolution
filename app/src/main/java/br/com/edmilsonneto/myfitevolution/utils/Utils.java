package br.com.edmilsonneto.myfitevolution.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

    public static boolean loadAtivo;

    public static void showAlert(final Activity activity, final String title, final String msg) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setTitle(title);
                builder.setMessage(msg);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.dismiss();
                    }
                });
                AlertDialog alerta = builder.create();
                alerta.show();
            }
        });
    }

    public static boolean isValidEmailAddress(String emailAddress) {
        Pattern p = Pattern.compile("^(([\\w-\\s]+)|([\\w-]+(?:\\.[\\w-]+)*)|([\\w-\\s]+)([\\w-]+(?:\\.[\\w-]+)*))(@((?:[\\w-]+\\.)*\\w[\\w-]{0,66})\\.([a-z]{2,6}(?:\\.[a-z]{2})?)$)|(@\\[?((25[0-5]\\.|2[0-4][0-9]\\.|1[0-9]{2}\\.|[0-9]{1,2}\\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\\]?$)");
        Matcher m = p.matcher(emailAddress.trim());
        return m.find();
    }
}
