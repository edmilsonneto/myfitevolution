package br.com.edmilsonneto.myfitevolution.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.edmilsonneto.myfitevolution.R;

public class ExemploFragment extends BaseFragment {

    public ExemploFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_exemplo, container, false);
        return view;
    }

}
